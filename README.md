**So, you have become an Ambassador**

Ambassadors have a very important role as they represent the server to the players, and are often the first contact a new player has with the server. So you need to be kind, helpful, and friendly! As an Ambassador you will have to deal with all kinds of people, and a lot of difficult situations; however most of the time you can just play the game as you always have been while keeping an eye on chat. Of course, You should also keep an eye out for glitches/cheats and market problems, and report them to a mod/admin. Your job is to help new players with basic problems, like learning the commands on a server. Your job is NOT, to correct spelling, argue, fight with players, or make fun of players. Players should think of you as a helping friend, NOT a robotic chat Nazi. We want to stop bullies not be bullies.

You NEED to be the calming force on the server, so don't let name calling or rudeness get to you. If things get crazy, ask a mod or seniorambassador for help. Mutes, or the threat of mute, is one of the ways an Ambassador controls chat, many times the threat of a mute will stop the bad behavior. Remember we don't WANT to mute our players, use this power with discretion. But sometimes you may need to mute right away, other times you may warn a player several times. As an example: if a player is spamming an IP address just mute and let a mod know, or if a player bypasses the chat filters with swearing warn a few times then mute, then explain the rules and consider unmuting. HOWEVER, if a player uses a 'slur' as an adjective, or spams a question over and over, warnings should be used before a mute.

An important part of the Ambassador job is getting to know people. You will make many friends in your time as staff, and this friendship is important. Getting to meet a wide variety of people is the most fun part of being a staff member, and is the reason we play on Muttsworld, to have fun playing a game with a bunch of friends. Something as simple as telling a player that their building looks amazing can really make their day. Remember that behind every player is a person who may be young, old, happy, or going through a rough time. Take the time to talk to people and be a good listener. Ambassadors are the gateway between staff and the community, and remember to treat players with respect and kindness.  

On a more serious note...

**Spam and other chat infractions**

DO NOT GET RILED UP ABOUT SPAM. It is a waste of your time and will make the players dislike you if you keep warning them about it. Players get excited when their friends join the server, which is good. Your job is to encourage this type of behavior, not stop it. If a player greets their friend in caps, "HELLOOOOOOOOOOOOOOOOOOOOOOOOO!!!", or wants to tell them something exciting, "I JUST FOUND THE BEST OCEAN TEMPLE EVER!!!!!!!!!!!!", you shouldn't see it as any form of rule breaking. On the other hand...

Bypassing the word filters is the main thing that you need to keep a look out for. People find all kinds of creative ways to spell curse words, using asterisks or any number of other tricks. A warning or two followed by a mute is a easy way to get the point across that this is not tolerated.

The last chat misdemeanor you should dedicate your time to resolving is global arguments--players will often find themselves frustrated or angered by one of their fellow players and take it out on them in global, we do not tolerate bullying. If a player is demeaning another player, you need to step in. If you feel that it is a mutual argument, ask them to redirect the conversation to a private message or local chat before issuing a warning/mute. Your main chat moderation goal is to keep things that are said clean and kind as we want to keep MuttsWorld a safe environment for those of all ages.

!!!WARNING A PLAYER SHOULD BE DONE VERBALLY AND WITH COMMANDS/THROUGH THE GUI!!! </warn username reason>
		

**Extras:**

So you log on to your favorite server and there are 2-3 other Ambassadors on and not much to do... If you have 'Ambassador' on one of our other servers this would be the perfect time to go there and see if you can help. If you log on to a server and it is full of players and the Ambassadors are overwhelmed, most of the time you can help even if you are not a Ambassador on that server.

Some tips to live by: - Always use "please" and "thank you" when moderating chat. Makes the players feel respected.
- If a player is disobeying the chat rules, don't just say "caps," or "spam." Tell the player what they are doing wrong with manners.
- A player gets 1 warning, then its a mute, not a long mute make it 2-4 minutes depending on the severity. It's to show that we aren't going to sit around and give warnings all day. - A player will sometimes ask to buy or sell items in global chat. Kindly direct them to the trade chat for this purpose. Likewise, if a player is holding a non-trade related conversation in the trade chat, request that they move to global or local chat to carry on their conversation.

DISCORD: As you may know, MuttsWorld has a discord (https://discord.gg/52gCnXV), not only do we encourage that you use it to be more connected with the community, but require it as a method of communcation amongst staff members. You will be provided with the 'Ambassador' role on the discord as well as the server, the role gives you access to a #ambassador chat channel in which you should post any questions or concerns as well as stay in the loop of updates and possible changes that may effect your staffing. Although you can post any questions in the in-game staff chat, it is a common occurance that other online staff are AFK or simply don't see your message--posting it on discord ensures you an answer and possibly clears up the same confusion another staff memeber may have. This brings me to my next topic:

CoC (Chain of Command): As you probably already know, there are different variations of staff members, all ranking in different levels of authority. (Ambassador > SeniorAmbassador > Mod > SeniorMod > Admin > NetworkAdmin > Mumble). Always go to the person following your rank according to the Chain of Command if they are available. If you have a question about a command or a server setting, you should never assume that someone doesn't know the answer, if you have a question about something on the server, it's likely someone has priorly had the same question.

**Commands:**

Mutes can be issued per-channel or in all channels--we encourage you to always mute a player in the channel they are causing chaos in rather than all of them if you can. Per-channel mutes serve as a 'slap-on-the-wrist' and lets the player know that you serious about enforcing the rules.

Referring back to the example of repetitive death cries; if a player is spamming the same message over and over again in a certain channel and disregarding any given warning, then mute them in THAT specific channel before denying them use of all of them. It isn't uncommon that players simply aren't aware of the rules and can't see your warning over their repetitive spamming...

Although we prefer you warn players before muting them, it can't always work out that way. There will be those who come on to the server just to wreck havoc and break rules, and those are the ones who don't deserve a warning. If a player is spamming an IP or trying to bypass the filter repeatedly, you can go ahead and mute them and inform them of the rule(s) they broke through /msg <username>.

- Per-channel Mutes: /mute <username> <channel> <time>
- All-channel Mutes: /mpa <username> 
- Per-channel Unmutes: /unmute <username> <channel>
- All-channel Unmutes: /umpa <username>

<> If you do not specify a time, the mute will last until the next server restart--always specify a time. +MPA MUTES WILL ALWAYS LAST UNTIL RESTART+ Mpa mutes don't support time specificitity.


See who is in a channel: /chwho <channelname> (If a player is complaining about an invisible player harrassing them, you can inform them of this command.)

**Modreqs:**

Ambassadors are encouraged to handle any modreqs that can be performed without the need of a mod or admin (Questions, Accidental tickets, etc.). This will help the mods by clearing out unimportant tickets.

- To check an interface of all the open help tickets, use /check (click on any specific ticket to view specifics and options.)
- Use /amr reply # <reply> to reply to a ticket without closing it.
- Use /done # to close a ticket. 
- If you accidentally closed the wrong ticket, you can always /amr reopen # 
- To teleport to the location of a ticket, use /amr tp #

<> More commands can be found with /amr help 

<> Most if not all commands can be done through </check> GUI


An important role as an Ambassador is to help and welcome new players to the server. This may require you to visit the newest region every now and then and ask "Hello, everyone ok?". Remember, players will want to stay if we can give them a reason to. /newestregion

Along the same lines of welcoming new players is your responsiblity to make them stay. Make the players feel at home and that this is a friendly server in which the staff cares for their players. For this reason we have a designated command which ambassadors are encouraged to use liberally:

/tep 

This command will allow you to teleport without request to any specified player. Use this command to visit players. Show up, ask them how they're doing, comment on their build, stay and talk awhile. This will help players to get to know you. IMPORTANT- Take social cues when visiting players. If a player is obviously busy building and does not want to talk, wish them nice day and move along. There is nothing worse than feeling annoyed because of overpersistant staff. Give players space when they want it.

Every now and then, you will get a player who is unknowingly talking in Global Chat, and will not listen to you when you ask them to switch to local chat. Theres a command for this! Remember to ask the player to move into local chat before forcing them to do so, as this will teach them how to switch channels if they don't already know. /fl <Playername> EX: /fl Brooke356

Lastly, and this goes with doing modreqs, ambassadors have the TEP command, allowing for you to be able to teleport to another player without them having to accept the request. This is useful when dealing with problems between 2 players, or, like previously stated, when dealing with moderns. /tep <Player name> EX: /tep Liebelegof (WARNING: If you /tep to a mod, you run the risk of dying, as we can either be over lava or in the air)

The last thing to remember is that Ambassadors are responsible for trying to sell diplomat and other store items, as well as getting players to vote. Ambassadors have all of the commands that permanent diplomats have. This includes /pwarps, the teleport compass, disguises, and /diploclub. Remember to use these things often when interacting with players, and when they ask how to get it, inform them it's one of the many perks of being diplomat and direct them to the store (/buydiplo) In addition, remind players that they make all grief stop by purchasing land protection. 

We thank you for your help and don't forget to have fun! :-)
